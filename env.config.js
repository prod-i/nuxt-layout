export default {
  /**
    * * Хост
  */
  host: process.env.HOST || '0.0.0.0',
  /**
    * * Порт
  */
  port: process.env.PORT || 4500,
  /**
    * * Путь до api
  */
  apiUrl: process.env.API_URL || '',
  /**
   * * Домен
  */
  siteDomain: process.env.SITE_DOMAIN || '',
  /**
   * * Режим разработки
  */
  devMode: process.env.DEVELOPMENT_MODE !== 'false'
}
