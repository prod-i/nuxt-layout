# nuxt-layout

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

```bash
# layouts - шаблоны страниц

# pages - страницы, названия файлов являются роутами


# Установка шаблона для определенной страницы:

#export default Vue.extend({
#
#  layout: 'signin'
#})

# Страницы по параметрам url устанавливаются через _
# Пример: _id.vue
```

```bash
# Основной файл scss подключается в nuxt.config 'styleResources'

# Остальные scss файлы подключаются к основному файлу стилей: @import 'blocks/style'
```

```bash
# Все статические данные находятся в папке static (моки данных, изображения, логотип)

```

```bash
# Svg Symbols находятся в compontns/ui/symbols
# Пример использования: 
#<svg class="header-list__icon">
#   <use xlink:href="#icon-plus" />
#</svg>

```
```bash
# ui компоненты (counter, input, paginations, checkbox) находяться в commponents/ui/common/
# Обьявляются глобально в файле plugins/common и подключается плагин в nuxt.config

```