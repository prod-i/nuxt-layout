import Vue from 'vue'
import UiInput from '@/components/ui/common/ui-input/ui-input'

const components = {
  UiInput
}

Object
  .entries(components)
  .forEach(([name, component]) => {
    Vue.component(name, component)
  })
