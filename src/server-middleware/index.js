import fs from 'fs'

function getPaths ({ name, path, children }) {
  return {
    name,
    path,
    children: Array.isArray(children) ? children.map(getPaths).filter(child => !!child.path) : []
  }
}

/**
 * @param {*} req
 * @param {import('http').ServerResponse} res
 * @param {*} next
 */
export default function (_, res) {
  fs.readFile('./.nuxt/routes.json', { encoding: 'utf8' }, (err, data) => {
    if (err) {
      res.writeHead(500, { 'Content-Type': 'application/json' })
      res.end('{ "message": "Can`t read the routes.json" }')
    }
    res.writeHead(200, { 'Content-Type': 'application/json' })

    const content = JSON.parse(data)

    const result = content.map(getPaths)
    res.end(JSON.stringify(result))
  })
}
