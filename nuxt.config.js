/* eslint-disable nuxt/no-cjs-in-config */
import path from 'path'

const srcDir = path.resolve(__dirname, 'src')

const config = {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
  */
  ssr: true,
  /*
   ** Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/style-resources',
    '@nuxtjs/color-mode'
  ],
  server: {
    host: '0.0.0.0',
    port: 5000
  },
  /*
   ** root folder of the project file structure
  */
  srcDir,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
   ** Global page headers: https://go.nuxtjs.dev/config-head
  */
  head: {
    title: 'Nuxt-Layout',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
   ** Global CSS: https://go.nuxtjs.dev/config-css
  */
  css: [
    '@/assets/scss/style.scss'
  ],
  styleResources: {
    scss: [
      '@/assets/scss/global.scss'
    ]
  },
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: false,
  loading: {
    color: '#E82F50',
    height: '5px'
  },
  /*
   ** Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  */
  plugins: [
    '~/plugins/axios-accessor.ts',
    '~/plugins/common.js'
  ],
  /**
   * See https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-servermiddleware
   */
  serverMiddleware: [
    { path: '/paths', handler: '~/server-middleware/index.js' }
  ],
  /*
   ** Modules: https://go.nuxtjs.dev/config-modules
  */
  modules: [
    '@nuxtjs/axios', // https://go.nuxtjs.dev/axios
    '@nuxtjs/style-resources', // Doc: https://www.npmjs.com/package/@nuxtjs/style-resources
    'cookie-universal-nuxt'
  ],
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    extractCSS: true
  },
  render: {
    static: {
      maxAge: 1000 * 60 * 60 * 20
    }
  },
  publicRuntimeConfig: {
    axios: {
      baseURL: '#####'
    }
  },
  privateRuntimeConfig: {
    axios: {
      baseURL: '#####'
    }
  }
}

export default config
